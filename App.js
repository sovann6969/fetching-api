/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';


import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Home from './src/component/screens/Home'
import ViewDetail from './src/component/screens/ViewDetail'
import FormAdd from './src/component/screens/FormAdd'
const Stack = createStackNavigator({
  Home: {
    screen: Home,
    title: 'TITLE',
    headerStyle :{justifyContent: 'center',borderWidth: 1, borderColor: '#1d2088'},
		headerTitleStyle :{textAlign: 'center', borderWidth: 1, borderColor: '#ff0000'}
	},
  AddForm:FormAdd,
  Detail:ViewDetail,

})

const App = createAppContainer(Stack);
export default App