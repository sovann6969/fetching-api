/**
 * @format
 */
import React, { Component } from 'react'
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import 'react-native-gesture-handler'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { store, persistor } from './src/store/store'
import Loading from './src/component/Loading';
import { Root } from 'native-base'

const App1 = () => (
    <Provider store={store}>
        <PersistGate persistor={persistor} loading={<Loading />}>
            <Root>
                <App />
            </Root>
        </PersistGate>
    </Provider>
)
AppRegistry.registerComponent(appName, () => App1);
