import React, { Component } from 'react'
import { Label, Button, Item, Input, Text, Icon } from 'native-base'
import { SafeAreaView, TouchableOpacity, View, Dimensions, StyleSheet, Image } from 'react-native'
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux'
import { addData, getData } from '../../action/action'
class FormAdd extends Component {
    static navigationOptions = {
        headerTitle: () => <Label>បញ្ចូលទិន្នន័យ</Label>,
        headerTitleStyle: {
            textAlign: "center",
            justifyContent: 'center',
            flex: 1
        },
    };
    constructor(props) {
        super(props);
        this.state = {
            filePath: {},
            title: '',
            dec: '',
            res: ''
        };
    }
    chooseFile = () => {
        var options = {
            title: 'Select Image',
            // customButtons: [
            //     { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            // ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                // let source = response;
                const source = { uri: response.uri };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    filePath: source, res: response
                });
            }
        });
    };
    changeDes = (text) => {
        this.setState({
            dec: text
        })
    }
    changeTitle = (text) => {
        this.setState({
            title: text
        })
    }
    submit = async () => {
        const { title, dec, res } = this.state;
        this.setState({
            title: '',
            dec: '',
            filePath: {},
            res: ''
        })
        this.props.addData(this.state);

        this.props.navigation.navigate('Home')
    }
    render() {
        return (
            <SafeAreaView>
                <View style={{ height: Dimensions.get('window').height / 2 + 100, width: Dimensions.get('window').width, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: 300, margin: 20 }}>
                        <Item floatingLabel style={{ marginTop: 10 }}>
                            <Icon name="ios-person" />
                            <Label>title</Label>
                            <Input onChangeText={this.changeTitle} />
                        </Item>
                        <Item floatingLabel style={{ marginTop: 10 }}>
                            <Icon name="ios-person" />
                            <Label>description</Label>
                            <Input onChangeText={this.changeDes} />
                        </Item>
                        <TouchableOpacity onPress={this.chooseFile.bind(this)} style={{ marginTop: 10 }}>
                            <Image
                                source={{ uri: this.state.filePath.uri || 'https://static.thenounproject.com/png/187803-200.png' }}
                                style={{ width: 300, height: 300 }} />
                        </TouchableOpacity>


                    </View>
                    <Button primary onPress={this.submit}><Text> បញ្ចូល </Text></Button>
                </View>
            </SafeAreaView>

        )
    }
}

export default connect(null, { addData, getData })(FormAdd)