import React, { Component } from 'react'
import { Text, View, VirtualizedList, YellowBox, TouchableOpacity } from 'react-native'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { connect } from 'react-redux'
import { deleteData, getData, searchData, getDataById } from '../../action/action'
import { Thumbnail, ActionSheet } from 'native-base'
import Icon from 'react-native-vector-icons/Ionicons'
import { withNavigation } from 'react-navigation'
var BUTTONS = ["View", "Delete", "Cancel"];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 2;
class Card extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false
        }
    }

    componentDidMount() {
        this.props.getData()
        this.setState({
            isLoading: true
        })
    }
    renderItem = ({ item }) => {
        YellowBox.ignoreWarnings([
            'VirtualizedLists should never be nested', // TODO: Remove when fixed
        ])

        return (
            <View >
                <TouchableOpacity onPress={() => { this.goToView(item.ID) }} onLongPress={() => {
                    ActionSheet.show(
                        {
                            options: BUTTONS,
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX,
                            title: "Choose What to do"
                        },
                        buttonIndex => {
                            if (buttonIndex == DESTRUCTIVE_INDEX) {
                                this.props.deleteData(item.ID);
                                this.props.getData()

                            }
                            else if (buttonIndex == CANCEL_INDEX) {

                            } else {
                                // view
                                this.goToView(item.ID)
                            }
                        }
                    )
                }}>
                    <View style={{ flex: 1, flexDirection: 'row', height: 150, padding: 10 }}>
                        <Thumbnail square style={{ height: 130, width: 130 }} source={{ uri: item.IMAGE || 'https://i.redd.it/3fzxdx8ou3911.jpg' }} />
                        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 10, backgroundColor: 'lightgrey' }}>
                            <Text style={{ fontSize: 20 }}> 
                                { ((item.DESCRIPTION).trim()).slice(0, 80) + '...'}
                            </Text>
                            <Text style={{ fontSize: 20 }}>
                                <Icon name="ios-clock" style={{ fontSize: 30 }}  ></Icon>  {(item.CREATED_DATE).slice(0, 4) + '-' + (item.CREATED_DATE).slice(4, 6) + '-' + (item.CREATED_DATE).slice(6, 8)}
                            </Text>
                        </View>
                    </View>

                </TouchableOpacity>
            </View>
        )
    }
    goToView = (id) => {
        this.props.getDataById(id)
        this.props.navigation.navigate('Detail')

    }
    goToHome = () => {

        this.props.navigation.navigate('Home')

    }
    render() {
        const data = this.props.articles
        console.log("why = ",data)
        return (
            <View>
                {this.state.isLoading ? (<VirtualizedList style={{}}
                    data={data}
                    getItem={(data, index) => data[index]}
                    getItemCount={() => data.length}
                    renderItem={this.renderItem}
                    windowSize={11}
                    removeClippedSubviews={true}
                    initialNumberToRender={20}
                    bounces={false}
                    keyExtractor={(item, index) => index.toString()}
                />) : (
                        [0, 1, 2, 3].map((_, index) => (
                            <View key={index} style={{ marginBottom: 12 }}>
                                <SkeletonPlaceholder>
                                    <View style={{ flexDirection: "row" }}>
                                        <View style={{ width: 100, height: 100 }} />

                                        <View
                                            style={{
                                                justifyContent: "space-between",
                                                marginLeft: 12,
                                                flex: 1
                                            }}
                                        >
                                            <View style={{ width: "50%", height: 20 }} />
                                            <View style={{ width: "30%", height: 20 }} />
                                            <View style={{ width: "80%", height: 20 }} />
                                        </View>
                                    </View>
                                </SkeletonPlaceholder>
                            </View>
                        ))
                    )
                }
            </View>
        )
    }
}

const mtp = (store) => {

    return {
        articles: store.articles,
    }
}
export default connect(mtp, { getData, deleteData, searchData, getDataById })(withNavigation((Card)))