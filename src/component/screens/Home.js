import React, { Component } from 'react'
import { Label, Button, Container, Header, Item, Input, Icon, Content } from 'native-base'
import { SafeAreaView, TouchableOpacity, View, Text, VirtualizedList, Image, StyleSheet, ScrollView } from 'react-native'
import { Pages } from 'react-native-pages'
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { connect } from 'react-redux'
import { getData, deleteData, searchData } from '../../action/action'
import Card from './Card'
const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 80,
        resizeMode: 'cover',
        flex: 1,
    },
    searchBar: {
        backgroundColor: 'lightgrey',
        borderRadius: 15
    },
    container: {
        width: '100%',
        height: 250,
        backgroundColor: '#E9EBEE',

    },
    container2: {
        margin: 10,
        width: '100%',

        backgroundColor: 'white',
    },


});
class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            keyword: ''
        }
    }


    static navigationOptions = ({ navigation }) => ({
        headerTitle: ()=>(<Label>បញ្ជីអត្ថបទ</Label>),
        headerRight: ()=>(
            <View style={{ marginRight: 10 }}>
                <Icon name='add' onPress={() => {
                    navigation.navigate('AddForm')
                }
                } />
            </View>
        ), headerTitleStyle: {
            textAlign: 'center',
            alignSelf: 'center'
        }

    });
    // componentDidMount() {
    //         // this.props.getData();
        
    // }
    componentDidUpdate(preProp, preState) {
        console.log(this.state.keyword,"key")
        if (preState.keyword !== this.state.keyword) {
            this.props.searchData(this.state.keyword)
        }
    }
    onTextChange = (text) => {
        this.setState({
            keyword: text
        })
    }
    render() {
        return (
            <View>
                <ScrollView>
                    {/* searchbar */}
                    <Header searchBar rounded style={styles.searchBar}>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="Type here..." onChangeText={this.onTextChange} />
                        </Item>
                    </Header>

                    <View style={styles.container}>
                        {/* content */}

                        <Pages
                            horizontal={true}
                            indicatorPosition='bottom'
                            indicatorColor='#FF9100'
                            indicatorOpacity={0.5}
                        >
                            <Image source={{ uri: 'https://coubsecure-s.akamaihd.net/get/b172/p/coub/simple/cw_timeline_pic/080419ce550/bc2bfb3fbd351099e9d00/med_1554034474_image.jpg' }} style={styles.image} />
                            <Image source={{ uri: 'https://i.ytimg.com/vi/J4fbjNUTGg4/hqdefault.jpg' }} style={styles.image} />
                            <Image source={{ uri: 'https://i.pinimg.com/originals/f9/84/2d/f9842d8b9f9a4ae974040aeb382df692.jpg' }} style={styles.image} />
                        </Pages>

                    </View>
                    <View style={styles.container2}>
                        <Card />
                    </View>
                </ScrollView>

            </View>
        )
    }

}
const mtp = (store) => {
    return {
        articles: store.articles,
        isSearching: store.isSearching
    }
}

export default connect(mtp, { getData, deleteData, searchData })(Home)
