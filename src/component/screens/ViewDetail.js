import React, { Component } from 'react'
import { Label, Button } from 'native-base'
import { SafeAreaView, TouchableOpacity, View, Image, Text, Dimensions } from 'react-native'
import { connect } from 'react-redux';
class Home extends Component {
    static navigationOptions = {
        title: "ពត៍មាន detail",
        headerTitleStyle: {
            textAlign: "center",
            justifyContent: 'center',
            flex: 1
        },
    };
    constructor(props) {
        super(props)

        this.state = {
            article: ''
        }
    }

    componentDidMount() {
        this.setState({
            article: this.props.article
        })

    }
    render() {

        return (
            <View>

                <View style={{ flex: 1, textAlign: 'center', textAlign: 'center', justifyContent: 'center' }} >
                    <Image style={{ width: Dimensions.get('window').width, resizeMode: 'cover', height: 600 }} source={{ uri: this.props.article.IMAGE || 'https://i.redd.it/3fzxdx8ou3911.jpg' }} />

                    <View style={{ flex: 1 }}>
                        <Label style={{ fontSize: 20, marginLeft: 10 }}>{this.props.article.TITLE}</Label>
                        <Text>{this.props.article.DESCRIPTION}</Text>
                    </View>
                </View>
            </View>
        )
    }
}
const mtp = (store) => {
    return {
        article: store.article
    }
}
export default connect(mtp)(Home)