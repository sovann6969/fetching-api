import React, { Component } from 'react'
import { Text, View ,Dimensions} from 'react-native'
import { Container, Header, Content, Spinner } from 'native-base'
export default class Loading extends Component {
    render() {
        return (
            <Container>
                <Header />
                <Content >
                    <View style={{ justifyContent: 'center',alignItems:'center',marginTop: Dimensions.get('window').height/2-100 }}>
                        <Spinner color='grey' />
                    </View>
                    <Text style={{justifyContent:'center',alignItems:'center',textAlign:'center'}}>Loading...</Text>
                </Content>
            </Container>
        )
    }
}
