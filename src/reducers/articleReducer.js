import { actionType } from '../action/actionType'
const initState = {
    articles: [],
    article: '',

}

export const articleReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.ADD_ARTICLE: {
            return {
                ...state, articles: action.payload
            }
        }
        case actionType.DELETE_ARTICLE: {
            return {
                ...state, articles: action.payload
            }
        }
        case actionType.GET_ARTICLE: {
            return {
                ...state, articles: action.payload
            }
        }
        case actionType.SEARCH_ARTICLE: {
            return {
                ...state, articles: action.payload
            }
        }
        case actionType.GET_AN_ARTICLE: {
            return {
                ...state, article: action.payload
            }
        }
        default: {
            return state
        }
    }
}