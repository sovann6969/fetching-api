import { createStore,applyMiddleware } from 'redux'
import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer, persistStore } from 'redux-persist'
import {articleReducer} from '../reducers/articleReducer'
import thunk from 'redux-thunk'
const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}
const perSistedReducer = persistReducer(persistConfig, articleReducer)
const store = createStore(
    perSistedReducer,
        applyMiddleware(thunk) 
)
const persistor = persistStore(store)
export { store, persistor }