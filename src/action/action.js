import { actionType } from './actionType'
import axios from 'axios'
export const getData = () => {
    return dispatch => {
        axios({
            method: 'GET',
            url: `http://110.74.194.124:15011/v1/api/articles?page=1&limit=100`,
        }).then((res) => {
            dispatch({ type: actionType.GET_ARTICLE, payload: res.data.DATA })
        })
    }
}
export const deleteData = (id) => {
    return dispatch => {
        axios({
            method: 'DELETE',
            url: 'http://110.74.194.124:15011/v1/api/articles/' + id,
        }).then((res) => {

        })
    }
}
export const searchData = (keyword) => {
    return dispatch => {
        axios({
            method: 'GET',
            url: 'http://110.74.194.124:15011/v1/api/articles?title=' + keyword + '&page=1&limit=15',
        }).then((res) => {
            dispatch({ type: actionType.SEARCH_ARTICLE, payload: res.data.DATA })
        })
    }
}
export const addData = (article) => {
    const { title, dec, res } = article;
    var file = new FormData()
    file.append('FILE', {
        name: res.fileName,
        type: res.type,
        uri: res.uri
    })
    return dispatch => {
        axios({
            method: 'POST',
            url: 'http://110.74.194.124:15011/v1/api/uploadfile/single',
            data: file
        }).then((res) => {

            axios({
                method: "POST",
                url: "http://110.74.194.124:15011/v1/api/articles",
                data: { TITLE: title, DESCRIPTION: dec, IMAGE: res.data.DATA }
            }).then((res) => {
            
                axios({
                    method: 'GET',
                    url: `http://110.74.194.124:15011/v1/api/articles?page=1&limit=100`,
                }).then((res) => {
                 
                    dispatch({ type: actionType.GET_ARTICLE, payload: res.data.DATA })
                })

            })
        }).catch((res) => {
            console.log('err', res)
        })

    }
}
export const getDataById = (id) => {

    return dispatch => {
        axios({
            method: 'GET',
            url: 'http://110.74.194.124:15011/v1/api/articles/' + id,
        }).then((res) => {
            dispatch({
                type: actionType.GET_AN_ARTICLE,
                payload: res.data.DATA
            })
        }).catch(res => {
            console.log(res)
        })
    }
}